import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {useColorScheme} from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';

import Navigator from './src/navigation';
import Theme from './src/constants/Theme';
import useCachedResources from './src/hooks/useCachedResources';

export default function App() {
  const colorScheme = useColorScheme();
  const {appIsReady} = useCachedResources(); 

  if(!appIsReady) return null;
  return (
    <SafeAreaProvider>
      <PaperProvider theme={Theme.currentTheme(colorScheme)}>
        <Navigator colorScheme={colorScheme}/>
      </PaperProvider>
      <StatusBar style="auto" />
    </SafeAreaProvider>
  );
}