function FirebaseConfig(mode='development'){
    let config ;
    switch(mode) {
        // case 'development':{config={};break;}
        // case 'staging':{config={};break;}
        // case 'production':{config={};break;}
        default: {
            config = {
                androidConfig:{
                    apiKey: "AIzaSyB_mf6ZOoeCEJPQAWnNwnrzgSZxb9GayGk",
                    authDomain: "randomizer-staging.firebaseapp.com",
                    databaseURL: "https://randomizer-staging-default-rtdb.firebaseio.com",
                    projectId: "randomizer-staging",
                    storageBucket: "randomizer-staging.appspot.com",
                    messagingSenderId: "568893069337",
                    appId: "1:568893069337:web:d51bbf99bacc2506a9f288",
                    measurementId: "G-YTZ922DVWW"
                },
                iosConfig:{
                    apiKey: "AIzaSyB_mf6ZOoeCEJPQAWnNwnrzgSZxb9GayGk",
                    authDomain: "randomizer-staging.firebaseapp.com",
                    databaseURL: "https://randomizer-staging-default-rtdb.firebaseio.com",
                    projectId: "randomizer-staging",
                    storageBucket: "randomizer-staging.appspot.com",
                    messagingSenderId: "568893069337",
                    appId: "1:568893069337:web:d51bbf99bacc2506a9f288",
                    measurementId: "G-YTZ922DVWW"
                },
                webConfig:{
                    apiKey: "AIzaSyB_mf6ZOoeCEJPQAWnNwnrzgSZxb9GayGk",
                    authDomain: "randomizer-staging.firebaseapp.com",
                    databaseURL: "https://randomizer-staging-default-rtdb.firebaseio.com",
                    projectId: "randomizer-staging",
                    storageBucket: "randomizer-staging.appspot.com",
                    messagingSenderId: "568893069337",
                    appId: "1:568893069337:web:d51bbf99bacc2506a9f288",
                    measurementId: "G-YTZ922DVWW"
                }
            }
            break;
        }
    }
    return config;
}

module.exports = FirebaseConfig;