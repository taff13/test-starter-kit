import { ExpoConfig, ConfigContext } from '@expo/config';
import AppJson from './config/app.json';
import StagingJson from './config/app.staging.json';
import ProductionJson from './config/app.prod.json';
import FirebaseConfig from './config/firebaseconfig';

type modeType = 'development' | 'staging' | 'production'

interface ConfigProps {
    config: ExpoConfig
}
export default () => {
  const mode:modeType = (process.env.mode as modeType) ?? 'development';  
  const useEmulator = process.env.emulator ?? false;
  
  let strings;
  let config:ExpoConfig;
  switch(mode) {
    case 'production':{
      config = (ProductionJson.expo as ExpoConfig);
      strings = {
        ...config.extra?.strings??null,
        appName: config.name,
        version: {
          android:config.version + '.' + config?.android?.versionCode??0,
          ios:config.version + '.' + config?.ios?.buildNumber??0,
          web:config.version
        },
      }
      break;
    }
    case 'staging':{
      config = (StagingJson.expo as ExpoConfig);
      strings = {
        ...config.extra?.strings??null,
        appName: config.name,
        version: {
          android:config.version + '.' + config?.android?.versionCode??0,
          ios:config.version + '.' + config?.ios?.buildNumber??0,
          web:config.version
        },
      }
      break;
    }
    default:{
      config = (AppJson.expo as ExpoConfig);
      strings = {
        ...config.extra?.strings??null,
        appName: config.name,
        version: {
          android:config.version + '.' + config?.android?.versionCode??0,
          ios:config.version + '.' + config?.ios?.buildNumber??0,
          web:config.version
        },
      }
      break;
    }
  }
  
  return {
    ...config,
    extra: {
        ...config.extra,
        firebaseConfig: FirebaseConfig(mode),
        mode:mode,
        strings:strings,
        useEmulator: useEmulator,
      },
  };
};