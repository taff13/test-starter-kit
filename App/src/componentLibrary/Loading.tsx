import React from 'react';
import { View } from 'react-native';
import {  ActivityIndicator, Modal, Portal, Text, useTheme } from 'react-native-paper';

interface LoadingModalType {
    visible: boolean
}

const containerStyle = {backgroundColor: 'transparent', width:'50%', padding: 20};

export function LoadingModal({visible}:LoadingModalType){ 
    const { colors } = useTheme();
    return(
        <Portal>
        <Modal 
            visible={visible} 
            style={{flex:1,justifyContent:'center',alignItems:'center'}} 
            contentContainerStyle={containerStyle}
        >
          <ActivityIndicator animating={true} color={colors.primary} size={'large'} />
        </Modal>
      </Portal>
    )
}

interface LoadingProps{
  size?: 'small' | 'large'
  visible: boolean
}
export function Loading({size='large',visible}:LoadingProps){ 
  const { colors } = useTheme();
  
  if(!visible) return <></>;
  return(
    <View style={size==='large'? {flex:1,justifyContent:'center',alignItems:'center'} : {alignItems:'center'}}>
    <ActivityIndicator animating={visible} color={colors.primary} size={size} />
    </View>
  )
}