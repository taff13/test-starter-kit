import React from 'react';
import {Platform, ScrollView, StyleSheet, View,} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export interface Props {
    children: any,
    scroll?:boolean,
    style?:{}
}
export default function MainView({children,scroll=true,style}:Props){ 
const styles = Styles();
    return(
        <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'}>
            {
                scroll?(
                    <ScrollView contentContainerStyle={style||styles.container} keyboardShouldPersistTaps={'always'}>
                        {children}
                    </ScrollView>
                )
                :(
                    <View style={style||styles.container}>
                        {children}
                    </View>
                )
            }
             
        </KeyboardAwareScrollView>
    )
}

const Styles = ()=>{
    const isDevice = (Platform.OS == 'ios' || Platform.OS == 'android');
    return (
        StyleSheet.create({
            container: {
                alignContent: 'center',
                alignSelf:'center',
                flex: 1,
                marginTop: isDevice?'40%': '5%',
                maxWidth: 700,
                paddingLeft: '10%',
                paddingRight: '10%',
                width: '100%',
            },
        })
    );
}