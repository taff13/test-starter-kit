import React from 'react';
import Strings from '../constants/Strings';
import { SidebarRoutes } from '../routes/SidebarRoutes.web';

export default function Sidebar(){ 
  const [sidebarToggled,setSidebarToggled] = React.useState(true);
  function toggleSidebar(){setSidebarToggled(!sidebarToggled)}
  

  
    return(
        <ul className={"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"+(sidebarToggled?'toggled':'')} id="accordionSidebar">
      {/* Sidebar - Brand */}
      <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div className="sidebar-brand-icon rotate-n-15">
          <img src={Strings.LOGO} title='Logo' style={{width:'35%', height:'35%'}} />
        </div>
        <div className="sidebar-brand-text mx-3">{Strings.APP_NAME}</div>
      </a>
      {/* Divider */}
      <hr className="sidebar-divider my-0" />
      {/* Nav Item - Dashboard */}
      <li className="nav-item active">
        <a className="nav-link" href="/dashboard">
          <i className="fas fa-fw fa-tachometer-alt" />
          <span>Dashboard</span></a>
      </li>
      {/* Divider */}
      <hr className="sidebar-divider" />
      {/* Heading */}
      {
        SidebarRoutes.map(({key,label,menu})=>{
          return(
            <div key={key}>
              <div className="sidebar-heading">{label}</div>
              {/* Nav Item - Pages Collapse Menu */}
              {
                menu.map(({key,label,link,subMenu})=>{
                  return(
                    <li key={key} className="nav-item">
                      {
                        link?(
                          <a className="nav-link" href={link}>
                            <i className="fas fa-fw fa-table" />
                            <span>{label}</span>
                          </a>
                        )
                        :(
                          <>
                            <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                              <i className="fas fa-fw fa-cog" />
                              <span>{label}</span>
                            </a>
                            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                              <div className="bg-white py-2 collapse-inner rounded">
                                <h6 className="collapse-header">{label}</h6>
                                {
                                  subMenu.map(({key,label,link})=>(
                                    <a key={key} className="collapse-item" href={link}>{label}</a>
                                  ))
                                }
                              </div>
                            </div>
                          </>
                        )
                      }
                    </li>
                  )
                })
              }
              {/* Divider */}
              <hr className="sidebar-divider" />
            </div>
          )
        })
      }
      {/* Sidebar Toggler (Sidebar) */}
      <div className="text-center d-none d-md-inline">
        <button className="rounded-circle border-0" id="sidebarToggle" onClick={toggleSidebar} />
      </div>
    </ul>
    )
}