import React from 'react';
import {Platform, ScrollView, StyleSheet,View,} from 'react-native';
import {Button,HelperText,Subheading,Text,TextInput,useTheme} from 'react-native-paper';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Formik} from 'formik';
import { useAuthentication } from '../hooks/useAuthentication';
import { signUpSchema } from '../constants/Schemas';
import { ScreenProps } from '../constants/GlobalTypes';

const InitialFormValues = {
    email: '',
    password: '',
}

export default function Signup({navigation}:ScreenProps){ 
    const {colors} = useTheme();
    const styles = Styles(colors);
    const {Signup} = useAuthentication({});
    const emailRef = React.useRef<any>();
    const passwordRef = React.useRef<any>();

    return(
        <KeyboardAwareScrollView
            contentContainerStyle={styles.container}
            extraScrollHeight={160} 
            keyboardShouldPersistTaps={'always'}
        >
            <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'always'}>
            <Formik
                    initialValues={InitialFormValues}
                    validationSchema={signUpSchema}
                    onSubmit={(values,actions)=>{
                        actions.setSubmitting(true);
                        Signup({...values})
                        .then(()=>{
                            actions.setSubmitting(false);
                            alert('Signed up');
                        })
                        .catch((error)=>{
                            actions.setFieldError('email',error.message);
                            actions.setSubmitting(false);
                        })
                    }}
                >
                    {({handleChange,handleSubmit,setFieldValue,setSubmitting,isSubmitting,values,errors})=>{
                        return(
                            <View>
                                <View style={styles.welcomeTextContainer}>
                                <Subheading style={styles.welcomeTextHeader}>Sign Up</Subheading>
                                </View>

                                <TextInput
                                    ref={(ref)=>emailRef.current=ref}
                                    mode={'flat'}
                                    style={styles.textInput}
                                    onChangeText={(email)=>setFieldValue('email',email.trim())}
                                    value={values.email}
                                    label={'Email'}
                                    placeholder={'Enter your email'}
                                    error={errors.email?true:false}
                                    //@ts-ignore
                                    autoCompleteType="email"
                                    returnKeyType={'next'}
                                    onSubmitEditing={()=>passwordRef?.current?.focus()}
                                />
                                {/*@ts-ignore*/}
                                <HelperText type={'error'} visible={true}>{errors.email}</HelperText>

                                {/*@ts-ignore*/}
                                <TextInput
                                    ref={(ref)=>passwordRef.current=ref}
                                    mode={'flat'}
                                    style={styles.textInput}
                                    onChangeText={handleChange('password')}
                                    value={values.password}
                                    label={'Password'}
                                    placeholder={'Enter your password'}
                                    error={errors.password?true:false}
                                    secureTextEntry={true}
                                    returnKeyType={'done'}
                                    onSubmitEditing={()=>handleSubmit()}
                                />
                                {/*@ts-ignore*/}
                                <HelperText type={'error'} visible={true}>{errors.password}</HelperText>

                                {/*@ts-ignore*/}
                                <Text onPress={()=>{navigation.navigate('Docs',{doc:'termsandconditions'})}}>By signing up you are agreeing to our terms and conditions</Text>

                                <Button
                                    style={styles.button}
                                    onPress={()=>handleSubmit()}
                                    color={colors.primary}
                                    mode={'contained'}
                                    disabled={isSubmitting}
                                    loading={isSubmitting}
                                    uppercase={true}
                                >
                                    Signup
                                </Button>

                                {navigation.canGoBack() && <Button
                                    style={styles.button2}
                                    labelStyle={{fontSize: 14,}}
                                    onPress={()=>navigation.goBack()}
                                    color={colors.primary}
                                    mode={'text'}
                                    disabled={isSubmitting}
                                    loading={isSubmitting}
                                    uppercase={false}
                                >
                                    Go Back
                                </Button>}
                            </View>
                        )
                    }}
                </Formik>
            </ScrollView>
        </KeyboardAwareScrollView>
    );
}

const Styles = (colors:ReactNativePaper.ThemeColors)=>{
    const isDevice = (Platform.OS == 'ios' || Platform.OS == 'android')
    return (
        StyleSheet.create({
            bottomsheetContainer: {
                alignContent: 'center',
                flex: 1,
                marginTop: '5%',
                paddingLeft: '10%',
                paddingRight: '10%',
                width: '100%',
            },
            button: {
                borderRadius: 10,
                height: 55,
                justifyContent: 'space-around',
                marginTop: '4%',
            },
            button2: {
                borderRadius: 10,
                height: 55,
                justifyContent: 'space-around',
                marginTop: '4%',
            },
            container: {
                alignContent: 'center',
                alignSelf: 'center',
                flex: 1,
                marginBottom: 50,
                marginTop: isDevice?'20%': '5%',
                maxWidth: 700,
                paddingLeft: '10%',
                paddingRight: '10%',
                width: '100%',
            },
            errorText: {
                color: colors.error,
            },
            forgotPassword: {
                alignSelf: 'flex-end',
                marginTop: -20
            },
            logo: {
                height: 36,
                marginBottom: '20%',
                resizeMode: 'contain',
                width: 108,
            },
            textInput: {
                fontSize: 17,
                marginBottom: 10,
            },
            welcomeText: {
                color: '#999999',
                fontSize: 14,
            },
            welcomeTextContainer: {
                marginBottom: '5%'
            },
            welcomeTextHeader: {
                fontSize: 20,
                fontWeight: 'bold',
            },
        })
    );
}