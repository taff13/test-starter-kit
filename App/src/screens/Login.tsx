import React from 'react';
import {Platform, ScrollView, StyleSheet,View} from 'react-native';
import {Button,HelperText,Subheading,TextInput,useTheme} from 'react-native-paper';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Formik} from 'formik';
import { useAuthentication } from '../hooks/useAuthentication';
import { signInSchema } from '../constants/Schemas';
import { ScreenProps } from '../constants/GlobalTypes';
import MainView from '../componentLibrary/MainView';

const InitialFormValues = {
    email: '',
    password: ''
}

export default function Login({navigation}:ScreenProps){ 
    const {colors} = useTheme();
    const styles = Styles(colors);
    const {Login,LoginWithGoogle} = useAuthentication({});
    const passwordRef = React.useRef<any>();

    return(
        <MainView>
                <Formik
                    initialValues={InitialFormValues}
                    validationSchema={signInSchema}
                    onSubmit={(values,actions)=>{
                        Login({email:values.email,password:values.password})
                        .then(()=>{
                            actions.setSubmitting(false);
                            alert('Logged in');
                        })
                        .catch((error)=>{
                            actions.setFieldError('email',error.message);
                            actions.setSubmitting(false);
                        })
                    }}
                >
                    {({handleChange,handleSubmit,isSubmitting,values,errors})=>{
                        return(
                            <View>
                                <View style={styles.welcomeTextContainer}>
                                    <Subheading style={styles.welcomeTextHeader}>Login</Subheading>
                                </View>

                                <TextInput
                                    mode={'flat'}
                                    style={styles.textInput}
                                    onChangeText={handleChange('email')}
                                    value={values.email}
                                    label={'Email'}
                                    placeholder={'Enter your email'}
                                    error={errors.email?true:false}
                                    //@ts-ignore
                                    autoCompleteType="email"
                                    returnKeyType={'next'}
                                    onSubmitEditing={()=>passwordRef?.current?.focus()}
                                />
                                {/*@ts-ignore*/}
                                <HelperText type={'error'} visible={true}>{errors.email}</HelperText>

                                {/*@ts-ignore*/}
                                <TextInput
                                    ref={(ref)=>passwordRef.current=ref}
                                    mode={'flat'}
                                    style={styles.textInput}
                                    onChangeText={handleChange('password')}
                                    value={values.password}
                                    label={'Password'}
                                    placeholder={'Enter your password'}
                                    error={errors.password?true:false}
                                    secureTextEntry={true}
                                    returnKeyType={'done'}
                                    onSubmitEditing={()=>handleSubmit()}
                                />
                                {/*@ts-ignore*/}
                                <HelperText type={'error'} visible={true}>{errors.password}</HelperText>

                                <Button
                                    style={styles.forgotPassword}
                                    labelStyle={{fontSize: 12}}
                                    onPress={()=>navigation.navigate('Forgot Password')}
                                    color={colors.primary}
                                    mode={'text'}
                                    disabled={isSubmitting}
                                    loading={isSubmitting}
                                    uppercase={false}
                                >
                                    Forgot Password?
                                </Button>

                                <Button
                                    style={styles.button}
                                    onPress={()=>handleSubmit()}
                                    color={colors.primary}
                                    mode={'contained'}
                                    disabled={isSubmitting}
                                    loading={isSubmitting}
                                    uppercase={true}
                                >
                                    Login
                                </Button>
                            </View>
                        )
                    }}
                </Formik>
            </MainView>
    )
}

const Styles = (colors:any)=>{
    const isDevice = (Platform.OS == 'ios' || Platform.OS == 'android')
    return (
        StyleSheet.create({
            button: {
                borderRadius: 10,
                height: 55,
                justifyContent: 'space-around',
                marginTop: '4%',
            },
            button2: {
                borderRadius: 10,
                height: 55,
                justifyContent: 'space-around',
                marginTop: '4%',
            },
            container: {
                alignContent: 'center',
                alignSelf:'center',
                flex: 1,
                marginTop: isDevice?'40%': '5%',
                maxWidth: 700,
                paddingLeft: '10%',
                paddingRight: '10%',
                width: '100%',
            },
            errorText: {
                color: colors.error,
            },
            forgotPassword: {
                alignSelf: 'flex-end',
                marginTop: -20
            },
            logo: {
                height: 36,
                marginBottom: '20%',
                resizeMode: 'contain',
                width: 108,
            },
            textInput: {
                fontSize: 17,
                marginBottom: 20,
            },
            welcomeText: {
                color: '#999999',
                fontSize: 14,
            },
            welcomeTextContainer: {
                marginBottom: '20%'
            },
            welcomeTextHeader: {
                fontSize: 20,
                fontWeight: 'bold',
            },
        })
    );
}