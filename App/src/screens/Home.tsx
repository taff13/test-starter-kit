import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Divider, Headline, List, Subheading, useTheme } from 'react-native-paper';
import { ScreenProps } from '../constants/GlobalTypes';

export default function Home({navigation}:ScreenProps) {
  const theme = useTheme();
  const {colors} = theme;
  const styles = Styles(colors);
  return (
    <View style={styles.container}>
      <Headline>Demo Pages:</Headline>
      <List.Section>
        <Subheading>Authentication</Subheading>
        <List.Item title="Login" onPress={()=>navigation.navigate('Login')}/>
        <Divider />
        <List.Item title="Login Phone" onPress={()=>navigation.navigate('LoginPhone')}/>
        <Divider />
        <List.Item title="Sign up" onPress={()=>navigation.navigate('Signup')}/>
        <Divider />
        <List.Item title="Forgot Password" onPress={()=>navigation.navigate('Forgot Password')}/>
      </List.Section>
      
      <List.Section>
        <Subheading>Webview</Subheading>
        <List.Item title="Terms and Conditions" onPress={()=>navigation.navigate('Docs',{doc:'termsandconditions'})}/>
      </List.Section>
      <Divider />
    </View>
  );
}

const Styles = (colors:ReactNativePaper.ThemeColors)=>(
  StyleSheet.create({
    container: {
      alignContent: 'center',
      alignSelf: 'center',
      flex: 1,
      marginBottom: 50,
      maxWidth: 700,
      paddingLeft: '10%',
      paddingRight: '10%',
      width: '100%',
  },
  })
);