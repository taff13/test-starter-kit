import React from 'react';
import DashboardLayout from '../layouts/DashboardLayout.web';

export default function Dasboard() {

    function StatsRow({ }) {
        const Stats = [
            {key:0,title:"Earnings (Monthly)",titleColor:"text-primary",description:"$40,000",desColor:"text-gray-800",icon:<i className="fas fa-calendar fa-2x text-gray-300" />,cardBorder:"border-left-primary"},
            {key:1,title:"Earnings (Annual)",titleColor:"text-success",description:"$215,000",desColor:"text-gray-800",icon:<i className="fas fa-dollar-sign fa-2x text-gray-300" />,cardBorder:"border-left-success"},
            {key:2,title:"Earnings (Monthly)",titleColor:"text-info",description:"$40,000",desColor:"text-gray-800",icon:<i className="fas fa-clipboard-list fa-2x text-gray-300" />,cardBorder:"border-left-info"},
            {key:3,title:"Earnings (Monthly)",titleColor:"text-warning",description:"$40,000",desColor:"text-gray-800",icon:<i className="fas fa-comments fa-2x text-gray-300" />,cardBorder:"border-left-warning"}
        ];
        return (
            <div className="row">
                {
                    Stats.map((item)=>{
                        return(
                            <div key={item.key} className="col-xl-3 col-md-6 mb-4">
                                <div className={"card "+item.cardBorder+" shadow h-100 py-2"}>
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className={"text-xs font-weight-bold "+item.titleColor+" text-uppercase mb-1"}>{item.title}</div>
                                                <div className={"h5 mb-0 font-weight-bold "+item.desColor}>{item.description}</div>
                                            </div>
                                            <div className="col-auto"> {item.icon} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        );
    }


    function Projects({ }) {
        return (<div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">Projects</h6>
            </div>
            <div className="card-body">
                <h4 className="small font-weight-bold">Server Migration <span className="float-right">20%</span></h4>
                <div className="progress mb-4">
                    <div className="progress-bar bg-danger" role="progressbar" style={{
                        width: '20%'
                    }} aria-valuenow={20} aria-valuemin={0} aria-valuemax={100} />
                </div>
                <h4 className="small font-weight-bold">Sales Tracking <span className="float-right">40%</span></h4>
                <div className="progress mb-4">
                    <div className="progress-bar bg-warning" role="progressbar" style={{
                        width: '40%'
                    }} aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} />
                </div>
                <h4 className="small font-weight-bold">Customer Database <span className="float-right">60%</span></h4>
                <div className="progress mb-4">
                    <div className="progress-bar" role="progressbar" style={{
                        width: '60%'
                    }} aria-valuenow={60} aria-valuemin={0} aria-valuemax={100} />
                </div>
                <h4 className="small font-weight-bold">Payout Details <span className="float-right">80%</span></h4>
                <div className="progress mb-4">
                    <div className="progress-bar bg-info" role="progressbar" style={{
                        width: '80%'
                    }} aria-valuenow={80} aria-valuemin={0} aria-valuemax={100} />
                </div>
                <h4 className="small font-weight-bold">Account Setup <span className="float-right">Complete!</span></h4>
                <div className="progress">
                    <div className="progress-bar bg-success" role="progressbar" style={{
                        width: '100%'
                    }} aria-valuenow={100} aria-valuemin={0} aria-valuemax={100} />
                </div>
            </div>
        </div>);
    }

    function Illustration({ }) {
        return (<div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">Illustrations</h6>
            </div>
            <div className="card-body">
                <div className="text-center">
                    <img className="img-fluid px-3 px-sm-4 mt-3 mb-4" style={{
                        width: '25rem'
                    }} src="img/undraw_posting_photo.svg" alt="..." />
                </div>
                <p>Add some quality, svg illustrations to your project courtesy of <a target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a
                    constantly updated collection of beautiful svg images that you can use
                    completely free and without attribution!</p>
                <a target="_blank" rel="nofollow" href="https://undraw.co/">Browse Illustrations on
                    unDraw →</a>
            </div>
        </div>);
    }

    return (
        <DashboardLayout title={'Dashboard'}>
            {/* Begin Page Content */}
            <div>
                {/* Content Row */}
                <StatsRow />
                {/* Content Row */}
                <div className="row">
                    {/* Content Column */}
                    <div className="col-lg-6 mb-4">
                        {/* Project Card Example */}
                        <Projects />
                    </div>
                    <div className="col-lg-6 mb-4">
                        {/* Illustrations */}
                        <Illustration />
                    </div>
                </div>
            </div>
            {/* /.container-fluid */}
        </DashboardLayout>

    )
}