import * as React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button, HelperText, TextInput, useTheme } from 'react-native-paper';
import { Formik } from 'formik';
import { useAuthentication } from '../hooks/useAuthentication';
import { ScreenProps } from '../constants/GlobalTypes';


export default function ForgotPassword({ navigation }:ScreenProps) {
  const { colors } = useTheme();
  const styles = Styles(colors);
  const { ResetPassword } = useAuthentication({});

  const handlePasswordReset = async (email: string, setSubmitting: (l: boolean) => void, setFieldError: (field: string, error: string) => void) => {
    setSubmitting(true);
    ResetPassword(email)
      .then(() => {
        alert('Password reset email sent successfully');
        navigation.goBack();
      })
      .catch((error) => {
        setFieldError('email', 'The entered email address is invalid.');
      })
      .finally(() => {
        setSubmitting(false);
      });
  }

  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.container}>
      <View style={{}}>
        <Text style={{ ...styles.text, color: colors.placeholder }}>Forgot Password?</Text>

        <Formik
          initialValues={{ email: '' }}
          onSubmit={(values, actions) => {
            handlePasswordReset(values.email, actions.setSubmitting, actions.setFieldError);
          }}
        >
          {
            ({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, }) => (
              <>
                {/*@ts-ignore */}
                <TextInput
                  value={values.email}
                  onChangeText={handleChange('email')}
                  placeholder='Enter your email'
                  label={'email'}
                  autoCapitalize='none'
                  error={errors.email ? true : false}
                  onSubmitEditing={() => handleSubmit()}
                />
                {/*@ts-ignore */}
                <HelperText type={'error'} visible={true}>{errors.email}</HelperText>
                
                <Button
                  style={styles.button}
                  onPress={() => handleSubmit()}
                  mode='contained'
                  disabled={!isValid || isSubmitting}
                  loading={isSubmitting}
                >
                  Send Email
                </Button>
                {navigation.canGoBack() && <Button
                    style={{ borderRadius: 10 }}
                    onPress={() => { navigation.goBack() }}
                    disabled={isSubmitting}
                  >
                    Go Back
                  </Button>}
              </>
            )
          }
        </Formik>
      </View>
    </KeyboardAwareScrollView>
  );
}

const Styles = (colors: any) => {
  const isDevice = (Platform.OS == 'ios' || Platform.OS == 'android');
  return (
    StyleSheet.create({
      buttonContainer: {
        margin: 25,
      },
      button: {
        borderRadius: 10,
        height: 55,
        justifyContent: 'space-around',
        marginTop: '15%',
      },
      container: {
        alignContent: 'center',
        alignSelf: 'center',
        flex: 1,
        marginTop: isDevice ? '40%' : '5%',
        maxWidth: 700,
        paddingLeft: '10%',
        paddingRight: '10%',
        width: '100%',
      },
      errorText: {
        color: colors.error,
      },
      logo: {
        height: 36,
        marginBottom: '20%',
        resizeMode: 'contain',
        width: 108,
      },
      signupContainer: {
        marginTop: '10%',
      },
      signupButton: {
        marginTop: '2%',
        height: 45,
        justifyContent: 'space-around',
      },
      text: {
        marginBottom: 15,
      },
    })
  );
}