import React from 'react';
import {Platform, StyleSheet,View} from 'react-native';
import {Button,HelperText,Subheading,TextInput,useTheme} from 'react-native-paper';
import {Formik} from 'formik';
import { FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha';
import { useAuthentication } from '../hooks/useAuthentication';
import { signPhoneSchema } from '../constants/Schemas';
import { ScreenProps } from '../constants/GlobalTypes';
import MainView from '../componentLibrary/MainView';
import { Firebase } from '../constants/Firebase';

const InitialFormValues = {
    phone: '',
    password: ''
}

export default function LoginPhone({navigation}:ScreenProps){ 
    const {colors} = useTheme();
    const styles = Styles(colors);
    const {verificationId,LoginWithNumber} = useAuthentication({});
    const recaptchaVerifier = React.useRef<any>(null);
    const [verificationCode, setVerificationCode] = React.useState('');

    const [message, showMessage] = React.useState({text:'',color:''});


    if(verificationId){
        return(
            <MainView>
                <Subheading style={{ marginTop: 20 }}>Enter Verification code</Subheading>
                {/*@ts-ignore*/}
                <TextInput
                    mode={'flat'}
                    style={styles.textInput}
                    editable={!!verificationId}
                    placeholder="'Enter verification code'"
                    onChangeText={setVerificationCode}
                    value={verificationCode}
                    label={'Verification Code'}
                />
                <Button
                    disabled={!verificationId}
                    onPress={async () => {
                        LoginWithNumber('verify',{code:verificationCode,recaptchaVerifier})
                        .then((message)=>{
                            showMessage({ text: message??'',color:'green' });
                        })
                        .catch((err)=>{
                            showMessage({ text: `Error: ${err.message}`, color: 'red' });
                        })
                    }}
                >
                    Confirm Verification Code
                </Button>
                {/*@ts-ignore*/}
                <HelperText type={'info'} visible={true}>{message.text}</HelperText>
            </MainView>
        );
    }

    return(
        <MainView>
                <Formik
                    initialValues={InitialFormValues}
                    validationSchema={signPhoneSchema}
                    onSubmit={async({phone},actions)=>{
                        LoginWithNumber('sendCode',{phone,recaptchaVerifier})
                        .then((message)=>{})
                        .catch((error)=>{
                            actions.setFieldError('phone',error.message);
                        })
                    }}
                >
                    {({handleChange,handleSubmit,isSubmitting,values,errors})=>{
                        return(
                            <View>
                                <View style={styles.welcomeTextContainer}>
                                    <Subheading style={styles.welcomeTextHeader}>Login</Subheading>
                                </View>

                                <TextInput
                                    mode={'flat'}
                                    style={styles.textInput}
                                    onChangeText={handleChange('phone')}
                                    value={values.phone}
                                    label={'Phone'}
                                    placeholder={'Enter your phone number'}
                                    error={errors.phone?true:false}
                                    //@ts-ignore
                                    autoCompleteType="phone"
                                    keyboardType={'phone-pad'}
                                    returnKeyType={'done'}
                                    onSubmitEditing={()=>handleSubmit()}
                                />
                                {/*@ts-ignore*/}
                                <HelperText type={'error'} visible={true}>{errors.phone||message.text}</HelperText>

                                <FirebaseRecaptchaVerifierModal
                                    ref={recaptchaVerifier}
                                    firebaseConfig={Firebase.options}
                                    attemptInvisibleVerification
                                    androidHardwareAccelerationDisabled
                                />

                                <Button
                                    style={styles.button}
                                    onPress={()=>handleSubmit()}
                                    color={colors.primary}
                                    mode={'contained'}
                                    disabled={isSubmitting}
                                    loading={isSubmitting}
                                    uppercase={true}
                                >
                                    Login
                                </Button>
                            </View>
                        )
                    }}
                </Formik>
            </MainView>
    )
}

const Styles = (colors:any)=>{
    const isDevice = (Platform.OS == 'ios' || Platform.OS == 'android')
    return (
        StyleSheet.create({
            button: {
                borderRadius: 10,
                height: 55,
                justifyContent: 'space-around',
                marginTop: '4%',
            },
            button2: {
                borderRadius: 10,
                height: 55,
                justifyContent: 'space-around',
                marginTop: '4%',
            },
            container: {
                alignContent: 'center',
                alignSelf:'center',
                flex: 1,
                marginTop: isDevice?'40%': '5%',
                maxWidth: 700,
                paddingLeft: '10%',
                paddingRight: '10%',
                width: '100%',
            },
            errorText: {
                color: colors.error,
            },
            forgotPassword: {
                alignSelf: 'flex-end',
                marginTop: -20
            },
            logo: {
                height: 36,
                marginBottom: '20%',
                resizeMode: 'contain',
                width: 108,
            },
            textInput: {
                fontSize: 17,
                marginBottom: 20,
            },
            welcomeText: {
                color: '#999999',
                fontSize: 14,
            },
            welcomeTextContainer: {
                marginBottom: '20%'
            },
            welcomeTextHeader: {
                fontSize: 20,
                fontWeight: 'bold',
            },
        })
    );
}