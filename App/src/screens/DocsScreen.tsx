import React from 'react';
import {Platform, StyleSheet,View,} from 'react-native';
import {useTheme} from 'react-native-paper';
import { WebView } from 'react-native-webview';
import { useDocs } from '../hooks/useDocs';
import { ScreenProps } from '../constants/GlobalTypes';
import { Loading } from '../componentLibrary/Loading';

export default function DocsScreen({navigation,route}:ScreenProps){ 
    const {doc: type} = route.params ?? {doc:''};
    const {colors} = useTheme();
    const styles = Styles(colors);

    const {loading,doc} = useDocs(type);

    React.useLayoutEffect(() => {
        let t = (type=='privacypolicy')?'Privacy Policy':(type=='termsandconditions')?'Terms & Conditions':'';
        navigation.setOptions({
          headerTitle: t,
          headerStyle: {
            backgroundColor: colors.background,
          },
          headerTintColor: colors.text,
        });
      }, [navigation]);

    return(
        <View style={styles.container}>
            <Loading size={'small'} visible={loading} />
            {
                Platform.OS === 'web'?
                <div dangerouslySetInnerHTML={{__html: doc}} />
                :
                <WebView 
                    source={{ html: '<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>'+doc+'</body></html>' }} 
                    originWhitelist={['*']} 
                />
            }
        </View>
    );
}

const Styles = (colors:any)=>{
    return (
        StyleSheet.create({
            container:{
                backgroundColor: 'white',
                flex: 1,
                height: '100%',
                padding: 20,
            },
        })
    );
}