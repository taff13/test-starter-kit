import {action,state} from './types';

export const reducer = (state: state, action: action) => {
    switch (action.type) {
        case 'logoutUser': {
            const isloggedIn = action.payload;
            return {...state, isloggedIn};
        }
        case 'setEmailVerified': {
            const emailVerified = action.payload;
            return {...state, emailVerified};
        }
        case 'setLogin': {
            const isloggedIn = action.payload;
            return {...state, isloggedIn};
        }
        case 'setUser': {
            const user = {...state.user, ...action.payload};
            return {...state, user};
        }
        case 'storeUserInfo': {
            const user = action.payload;
            return {...state,user};
        }
        case 'setUserInitial': {
            const {isloggedIn,user} = action.payload;
            return {...state, isloggedIn,user};
        }
    }
}
