import React from 'react';
import { UserType } from '../constants/GlobalTypes';

export interface action {
    type:   'setEmailVerified' |
            'setLogin' | 
            'setUser' | 
            'setUserInitial' | 
            'storeUserInfo' | 
            'logoutUser'
    payload: any
}

export interface Props {
    state: state;
    dispatch: React.Dispatch<action>;
}

export interface state {
    emailVerified: boolean;
    isloggedIn: boolean;
    user: UserType;
}

export const initialState:state = {
    emailVerified: false,
    isloggedIn: true,
    user: {}
}