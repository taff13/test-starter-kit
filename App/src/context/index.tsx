import React, { createContext, useReducer } from 'react';
import {Props,initialState} from './types';
import {reducer} from './reducer';

const AppContext = createContext<Props>({ state: initialState, dispatch: ()=>{} });

const AppContextProvider = (props: {children:React.ReactNode}) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
      <AppContext.Provider value={{state,dispatch}}>
        {props.children}
      </AppContext.Provider>
    );
};

const AppConsumer = AppContext.Consumer;

export { AppContext, AppContextProvider, AppConsumer };  