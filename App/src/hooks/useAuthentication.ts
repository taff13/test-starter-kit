import React from 'react';
import * as Updates from 'expo-updates';
import { FirebaseAuth } from '../constants/Firebase';
import { createUserWithEmailAndPassword, onAuthStateChanged, PhoneAuthProvider, sendEmailVerification, sendPasswordResetEmail, signInWithCredential, signInWithEmailAndPassword, signOut} from "firebase/auth";

const {useEffect,useState}=React;
 
export interface Props{checkLogin?:boolean}
export interface loginParams {email: string,password:string}
export interface loginGoogleParams {scope?:string[]}
export interface LoginWithNumberParams {phone?:string,code?:string,recaptchaVerifier:any}
export interface signupParams {email: string,password:string}

const InitialAuthState = {
    loading:false,
    role:'',
    isLoggedIn:false,
    isEmailVerified:false,
    verificationId:''
}
export const useAuthentication = ({checkLogin=false}:Props)=>{
    const [authState,setAuthState] = useState({...InitialAuthState,loading:true});
    
    const CheckLogin = async ()=>{
        return onAuthStateChanged(FirebaseAuth,async (user) => {
            if(user) {
                let isEmailVerified = false;
                if(!user.emailVerified){
                    pollVerification(user);
                }
                else {
                    isEmailVerified = true;
                }

                const role = await user.getIdTokenResult()
                .then((idTokenResult) => { 
                    const res =  (idTokenResult?.claims?.role??'' as string);
                    return (res as string);
                })
                .catch((error)=>{return ''})
                
                setAuthState({...InitialAuthState,role,isLoggedIn:true,isEmailVerified});
            }
            else {
                Logout();
                setAuthState(InitialAuthState);
            }
        });
    }
    const Login = async ({email,password}:loginParams)=>{
        return signInWithEmailAndPassword(FirebaseAuth, email, password)
        .then((userCredential) => {
            const user = userCredential.user;
        })
        .catch((error)=>{
            throw new Error("Invalid login credentials.");
        })
    }
    const LoginWithApple = async()=>{throw new Error('Currently unavailable');}
    const LoginWithGoogle = async({scope}:loginGoogleParams)=>{
        throw new Error('Currently unavailable');
        try{}
        catch(error){
            console.log('error: ', error);
            throw new Error("Invalid login credentials.");
        }
    }
    const LoginWithNumber = async(mode:'sendCode'|'verify',params:LoginWithNumberParams)=>{
        switch(mode) {
            case 'sendCode':{
                const {phone='0',recaptchaVerifier} = params;
                try {
                    const phoneProvider = new PhoneAuthProvider(FirebaseAuth);
                    const verificationId = await phoneProvider.verifyPhoneNumber(
                    phone,
                    recaptchaVerifier.current
                    );
                    setAuthState({...InitialAuthState,verificationId})
                    return 'Verification code has been sent to your phone.';
                } catch (err:any) {
                    throw new Error(err.message);
                }  
                return null;
            }
            case 'verify':{
                const {phone,code='0'} = params;
                const {verificationId} = authState;
                try {
                    const credential = PhoneAuthProvider.credential(verificationId,code);
                    await signInWithCredential(FirebaseAuth, credential);
                    return 'Phone authentication successful 👍';
                } 
                catch (err:any) {
                    throw new Error(err.message);
                }
                return;
            }
            
            default: throw new Error('Invalid mode');
        }
        return null;
    }
    const Logout = async ()=>{
        return signOut(FirebaseAuth);
    }
    const pollVerification = async (user:any) => {
        let timeout = 0;
        const poll = new Promise((resolve, reject) => {
          const pollInterval = setInterval(() => {
            user?.reload();
            const provider = user?.providerData[0]?.providerId;
            //console.log('provider: ', provider);
            const isVerified = (provider == 'password' || provider == 'phone') ? user?.emailVerified : true;
            //console.log('check Verifiy',isVerified);
            if (isVerified) {
              //clearInterval();
              //getUserData();
              resolve(pollInterval);
            }
            if (timeout >= 1000) {
              //timeout after 15minutes
              reject('time out');
            }
            timeout++;
          }, 1000);
        });
    
        poll.then(
          async function (value: any) {
            //console.log('resolved');
            clearInterval(value);
            await Updates.reloadAsync();
          },
          function (error) {
            console.log('error: ', error.message);
            clearInterval();
          }
        );
    };
    const ResetPassword = async (email:string) => {
        return sendPasswordResetEmail(FirebaseAuth,email);
    }
    const Signup = async ({email,password}:signupParams)=>{
        return createUserWithEmailAndPassword(FirebaseAuth, email, password)
        .then((userCredential) => {
          const user = userCredential.user;
          sendEmailVerification(user);
        })
        .catch(async (error:any)=>{
            throw new Error(error.message);
        });
    }
 
    useEffect(()=>{
        let unSubscribe: any = false;
        if(checkLogin) unSubscribe = CheckLogin();

        return ()=>{
            //if(unSubscribe) return unSubscribe();
        }
    },
    []);
    return {
        ...authState,
        CheckLogin,
        Login,
        LoginWithApple,
        LoginWithGoogle,
        LoginWithNumber,
        Logout,
        pollVerification,
        ResetPassword,
        Signup
    };
}