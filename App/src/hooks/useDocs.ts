import React from 'react';
import { Firestore } from '../constants/Firebase';
import { collection, doc, setDoc, getDoc } from "firebase/firestore"; 
const {useEffect,useState}=React;

const InitialState = {loading:false,doc:'',error:false,errorMessage:''};
export const useDocs = (type?:string)=>{
    const [state,setState] = useState(InitialState);

    const Getdoc = async(type:string)=>{
        setState({...InitialState,loading:true});
        const Ref = doc(Firestore,'Docs',type);
        return getDoc(Ref)
        .then((snapshot)=>{
            if(!snapshot.exists()){
                throw new Error('Failed to get document');
            }
            const doc = snapshot.data()?.text ?? '';
            setState({...InitialState,doc});
        })
        .catch((error)=>{
            setState({...InitialState,error:true,errorMessage:error.message});
        })
    }

    const UpdateDoc = async(type:string,text:string)=>{
        setState({...state,doc:text,loading:true});
        const Ref = doc(Firestore,'Docs',type);
        return setDoc(Ref,{
            text,
            updated: new Date().getTime()
        },
        {merge:true})
        .then(()=>{
            setState({...state,doc:text,loading:false});
            return text;
        });
    }

    useEffect(()=>{
        if(type)Getdoc(type);
    },[]);
    return {...state,Getdoc,UpdateDoc};
}