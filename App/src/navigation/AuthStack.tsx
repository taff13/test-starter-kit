import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { AuthRoutes as Routes } from '../routes/Routes';

const Stack = createStackNavigator();

export default function AuthStack(){ 
    return(
        <Stack.Navigator initialRouteName={ 'Login' }>
        {
            Routes.map((route)=>(
                <Stack.Screen 
                    key={route.key} 
                    name={route.name} 
                    component={route.component} 
                    options={route.options}
                />
            ))
        }
        </Stack.Navigator>
    )
}