import React from 'react';
import { DarkTheme, DefaultTheme, NavigationContainer } from '@react-navigation/native';
import * as Linking from 'expo-linking';
import { ColorSchemeName } from 'react-native';

import { RoutesConfig, } from '../routes/Routes';
import MainStack from './MainStack';
import SplashScreen from '../screens/SplashScreen';

const linking = {
    prefixes: [Linking.createURL('/')],
    config: RoutesConfig,
};

export default function Navigator({colorScheme,}:{colorScheme: ColorSchemeName;}){ 
    return(
        <NavigationContainer linking={linking} fallback={<SplashScreen/>} theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
            <MainStack />
        </NavigationContainer>
    )
}