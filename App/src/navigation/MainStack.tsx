import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { MainRoutes as Routes } from '../routes/Routes';
import AuthStack from './AuthStack';
import { AppContext } from '../context';

const Stack = createStackNavigator();

export default function MainStack(){ 
    const {state} = React.useContext(AppContext);
    const {isloggedIn} = state;
    return(
        <Stack.Navigator initialRouteName={ 'Home' }>
        {
            Routes.map(({key,name,component,options,isSecure})=>(
                <Stack.Screen 
                    key={key} 
                    name={name} 
                    component={ (isSecure && !isloggedIn)? AuthStack : component }
                    options={{
                        headerShown: !(isSecure && !isloggedIn),
                        ...options,
                    }}
                />
            ))
        }
        </Stack.Navigator>
    )
}