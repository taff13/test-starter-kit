export const SidebarRoutes = [
    {
      key:'accounts',
      label:'Accounts',
      menu:[
        {key:'users',label:'Users',link:'dashboard/users',icon:'',subMenu:[]},
      ]
    },
    {
      key:'utilities',
      label:'Utilities',
      menu:
      [
        {
          key:0,
          label:'Documents',
          link:null,
          icon:'',
          subMenu:[
            {key:'privacy',label:'Privacy Policy',link:'',},
            {key:'terms',label:'Terms & Conditions',link:'',},
          ]
        },
      ]
    },
  ];