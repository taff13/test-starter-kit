//add all WEB ONLY pages here

import Dashboard from "../screens/Dashboard.web";
import Users from "../screens/Users.web";


export const WebRouteConfig = {
    screens:{
        Home: '',
        Dashboard:'/dashboard',
        Users: '/dashboard/users'
    }
};

export type WebScreenParams = {
    Dashboard: undefined;
    Users: undefined;
}

export const WebOnlyRoutes = [
    {
        key: 'dashboard',
        component: Dashboard,
        isSecure: true,
        name: 'Dashboard',
        options: {headerShown:false},
    },
    {
        key: 'users',
        component: Users,
        isSecure: true,
        name: 'Users',
        options: {headerShown:false},
    },
];