import { Platform } from "react-native";

let ExtraRoutes:any = [];
let ExtraScreenParams = {};
let WebRouteConfig = {screens:{}};
if(Platform.OS === 'android' || Platform.OS === 'ios') { 
       const { MobileOnlyRoutes, MobileScreenParams } = require('./MobileOnlyRoutes');
       ExtraRoutes = MobileOnlyRoutes;
       ExtraScreenParams = MobileScreenParams;
}
else {
    const { WebOnlyRoutes, WebRouteConfig: config, WebScreenParams } = require('./WebOnlyRoutes');
    ExtraRoutes = WebOnlyRoutes;
    ExtraScreenParams = WebScreenParams;
    WebRouteConfig = config;
}

import DocsScreen from "../screens/DocsScreen";
import ForgotPassword from "../screens/ForgotPassword";
import Home from "../screens/Home";
import Login from "../screens/Login";
import LoginPhone from "../screens/LoginPhone";
import NotFound from "../screens/NotFound";
import Signup from "../screens/Signup";

type ScreenParamList = {
    Docs: {doc:'termsandconditions'|'privacypolicy'}
    Home: undefined;
    Login: undefined;
    LoginPhone: undefined;
    Signup: undefined;
    'Forgot Password': undefined;
};

const RoutesConfig = {
    screens:{
        ...WebRouteConfig?.screens,
        Docs: 'docs/:doc',
        'Forgot Password': 'forgotpassword',
        Login: 'login',
        LoginPhone: 'loginphone',
        Signup: 'signup',

        NotFound: '*', //404 page so must be the last item.
    }
};

//Add routes shared between web and mobile here
let AuthRoutes = [
    {
        key: 'ForgotPassword',
        component: ForgotPassword,
        isSecure: false,
        name: 'Forgot Password',
        options: {},
    },
    {
        key: 'Login',
        component: Login,
        isSecure: false,
        name: 'Login',
        options: {},
    },
    {
        key: 'LoginPhone',
        component: LoginPhone,
        isSecure: false,
        name: 'LoginPhone',
        options: {},
    },
    {
        key: 'Signup',
        component: Signup,
        isSecure: false,
        name: 'Signup',
        options: {},
    },
];
let MainRoutes = [
    {
        key: 'docs',
        component: DocsScreen,
        isSecure: false,
        name: 'Docs',
        options: {},
    },
    {
        key: 'home',
        component: Home,
        isSecure: false,
        name: 'Home',
        options: {},
    },
    //below is 404 screen so must be last item
    {
        key: 'NotFound',
        component: NotFound,
        isSecure: false,
        name: 'NotFound',
        options: {},
    },
    ...AuthRoutes
];

MainRoutes = [...MainRoutes,...ExtraRoutes];

export type MainScreenParamList = ScreenParamList & typeof ExtraScreenParams;
export {AuthRoutes,MainRoutes,RoutesConfig};