import { initializeApp } from "firebase/app";
import { getAuth, connectAuthEmulator } from "firebase/auth";
//import { getDatabase, connectDatabaseEmulator } from "firebase/database";
import { getFirestore, connectFirestoreEmulator } from "firebase/firestore";
import { getStorage, connectStorageEmulator } from "firebase/storage";
import { getFunctions, connectFunctionsEmulator } from 'firebase/functions';

import { LogBox, Platform } from 'react-native';
import Constants from 'expo-constants';

const {manifest} = Constants;
const idAddress = manifest?.debuggerHost?.split(':')[0] ?? 'localhost';
const useEmulator = manifest?.extra?.useEmulator ?? false;
const config = manifest?.extra?.firebaseConfig;

//made em separate to experiment with logging and analytics
let firebaseConfig = ()=>{
    switch(Platform.OS) {
        case 'android':{ return config.androidConfig; }
        case 'ios':{ return config.iosConfig; }
        default:{ return config.webConfig }
    }    
}


// Initialize Firebase
export const Firebase = initializeApp(firebaseConfig());
export const FirebaseAuth = getAuth(Firebase);
//export const database = getDatabase(app);
export const Firestore = getFirestore(Firebase);
export const FirebaseStorage = getStorage(Firebase);
export const FirebaseFunctions = getFunctions(Firebase);

if(useEmulator) {
    connectAuthEmulator(FirebaseAuth,"http://"+idAddress+":9099");
    //connectDatabaseEmulator(database,"http://"+idAddress,9000);
    connectFirestoreEmulator(Firestore, "http://"+idAddress, 8080);
    connectStorageEmulator(FirebaseStorage, "http://"+idAddress, 9199);
    connectFunctionsEmulator(FirebaseFunctions, "http://"+idAddress, 5001);
}




if( Platform.OS != 'web'  ) {
  LogBox.ignoreAllLogs(); //TODO: remember to resolve this
  const _console = { ...console };
  console.warn = message => {
    if (message.indexOf('Setting a timer') <= -1 && message.indexOf('VirtualizedLists should never be nested')<= -1) { _console.warn(message); }
    };
}