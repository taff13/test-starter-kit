import { DefaultTheme } from 'react-native-paper';

export const DarkTheme: ReactNativePaper.Theme = {
    ...DefaultTheme,
    dark: true,
    roundness: 4,
    colors: {
      ...DefaultTheme.colors,
      primary: '#4869CD',
      accent: '#03dac4',
      background: 'black',
      surface: 'black',
      error: '#B00020',
      text: 'white',
      onSurface: '#000000',
      disabled: '#7A7A7A',
      placeholder: '#7A7A7A',
      //backdrop: color('black').alpha(0.5).rgb().string(),
      //notification: 'pinkA400',
    },
};

export const LightTheme: ReactNativePaper.Theme = {
    ...DefaultTheme,
    dark: false,
    roundness: 4,
    colors: {
      ...DefaultTheme.colors,
      primary: '#4869CD',
      accent: '#03dac4',
      background: 'white',
      surface: 'white',
      error: '#B00020',
      text: '#1D1D1D',
      onSurface: '#000000',
      disabled: '#7A7A7A',
      placeholder: '#7A7A7A',
      //backdrop: color('black').alpha(0.5).rgb().string(),
      //notification: 'pinkA400',
    },
};

export const WebTheme: ReactNativePaper.Theme = {
    ...DefaultTheme,
    dark: false,
    roundness: 4,
    colors: {
      ...DefaultTheme.colors,
      primary: '#4869CD',
      accent: '#03dac4',
      background: 'white',
      surface: 'white',
      error: '#B00020',
      text: '#1D1D1D',
      onSurface: '#000000',
      disabled: '#7A7A7A',
      placeholder: '#7A7A7A',
      //backdrop: color('black').alpha(0.5).rgb().string(),
      //notification: 'pinkA400',
    },
};

export const currentTheme = ( mode: ('light' | 'dark' | null | undefined) )=>{
  switch(mode){
    case 'light':
      return LightTheme;
    case 'dark':
      return DarkTheme;
    default:
      return LightTheme    
  }
}

export default {
  currentTheme,
  DarkTheme,
  LightTheme,
  WebTheme,
}  