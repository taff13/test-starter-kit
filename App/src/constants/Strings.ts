import { Platform } from 'react-native';
import Constants from 'expo-constants';
const strings = Constants?.manifest?.extra?.strings;

export default {
    //apis
    //Empty messages
    //Error messages
    INVALID_EMAIL: 'You entered an invalid email address.',
    INVALID_PASSWORD: 'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character.',
    //Images
    LOGO: require('../../assets/icon.png'),
    SPLASH_IMAGE: require('../../assets/splash.png'),
    //Msc
    APP_NAME: strings.appName,
    LIST_LIMITS: 20,
    MAX_SCREEN_WIDTH: 1500,
    VERSION: Platform.OS==='android'? strings.version.android : Platform.OS==='ios'? strings.version.ios : strings.version.web,
}