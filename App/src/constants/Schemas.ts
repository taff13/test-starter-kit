import * as yup from 'yup';
import Strings from './Strings';

export const signUpSchema = yup.object().shape({
  email: yup.string().trim('white space').email(Strings.INVALID_EMAIL).required('Required'),
  password: yup.string().required('Required').matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?.<>,&])[A-Za-z\d@$!%*#?.<>,&]{8,}$/,
      Strings.INVALID_PASSWORD
    ),
});

export const signInSchema = yup.object().shape({
  email: yup.string().email(Strings.INVALID_EMAIL).required('Required'),
  password: yup.string().required('Required'),
});

export const signPhoneSchema = yup.object().shape({
  phone: yup.string().required('Required'),
});