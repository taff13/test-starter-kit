export function abbrNum(number:number) {
  // 2 decimal places => 100, 3 => 1000, etc
  let decPlaces = countDecemals(number);
  decPlaces = Math.pow(10,decPlaces);

  // Enumerate number abbreviations
  var abbrev = [ "k", "m", "b", "t" ];

  let numberString = '0';

  // Go through the array backwards, so we do the largest first
  for (var i=abbrev.length-1; i>=0; i--) {

      // Convert array index to "1000", "1000000", etc
      var size = Math.pow(10,(i+1)*3);

      // If the number is bigger or equal do the abbreviation
      if(size <= number) {
           // Here, we multiply by decPlaces, round, and then divide by decPlaces.
           // This gives us nice rounding to a particular decimal place.
           number = Math.round(number*decPlaces/size)/decPlaces;

           // Add the letter for the abbreviation
           numberString = number + abbrev[i];

           // We are done... stop
           break;
      }
  }

  return numberString;
}

export function capitalizeFirstLetter(string:string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
export function countDecemals (value:number) {
  if(Math.floor(value) === value) return 0;
  return value.toString().split(".")[1].length || 0; 
}

export const currencyFormatter = (number:number) => {
    const sign = number < 0 ? "-$" : "$";
    number = Math.abs(number);
    return sign +(number).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

export const formatPhoneNumber = (str:string) => {
  //Filter only numbers from the input
  let cleaned = ('' + str).replace(/\D/g, '');
  
  //Check if the input is of correct length
  let match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3]
  };

  return str;
};

export function isValidHttpUrl(string: string) {
  try {
    const url = new URL(string);
    if (url) return true;
    else return false;
  } catch (_) {
    return false;
  }
}