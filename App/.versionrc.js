const mode = process.env.mode? process.env.mode : 'development';
const build = process.env.build? process.env.build : 'both';

let fileName = './config/app.json';
switch (mode){
    case 'development': {
        fileName = './config/app.json';
        break;
    }
    case 'staging': {
        fileName = './config/app.staging.json';
        break;
    }
    case 'production': {
        fileName = './config/app.prod.json';
        break;
    }
    default: {
        fileName = './config/app.json';
        break;
    }
}

let bumpVersion = true;
let bumpFiles = [];
switch(build){
    case 'android': {
        bumpFiles = [
            {
                filename: fileName,
                updater: require.resolve('standard-version-expo/android/increment')
            }
        ];
        break;
    }
    case 'ios': {
        bumpFiles = [
            {
                filename: fileName,
                updater: require.resolve('standard-version-expo/ios/increment')
            }
        ];
        break;
    }
    default: {
        bumpVersion = false;
        bumpFiles = [
            {
                filename: fileName,
                updater: require.resolve('standard-version-expo')
            },
        ];
        break;
    }
}

module.exports = {
    bumpFiles: bumpFiles,
    packageFiles: [
        {
            filename: fileName,
            updater: require.resolve('standard-version-expo')
        },
    ]
}