#! /usr/bin/env node

const fs = require('fs');
const path = require('path');
const dir = path.resolve(__dirname);
const workingDir = process.cwd();
const readline = require('readline-sync');
const execSync = require('child_process').execSync;

let AppName = 'Randomizer';
let ProjectName;

async function init(){
    console.log( "Hello!");
    console.log( "This command tool will get you started.");
    
    AppName = readline.question(`What is your app name? `);
    console.log('Enter the name of the firebase project you have been added to in the firebase console.');
    ProjectName = readline.question(`Project Name: `);

    let res;
    console.log('Creating directory ....');
    res = execSync("mkdir "+AppName,{ encoding : 'utf8' });

    console.log('Copying files ....');
    res = execSync("cp -a "+dir+"/App/. "+AppName).toString();

    console.log('Creating app.json files ....');
    await EditConfigFiles();
    console.log('Setting up firebase ....');
    await HandleFirebase();
    console.log('Installing packages ....');
    await InstallNodeModules();
    console.log('DONE.');
    console.log('HAPPY CODING!!');
}

async function EditConfigFiles(){
    const app = workingDir+'/'+AppName+'/config/app.json';
    const staging = workingDir+'/'+AppName+'/config/app.staging.json';
    const production = workingDir+'/'+AppName+'/config/app.prod.json';

    const appFile = require(app);
    const stagingFile = require(staging);
    const prodFile = require(production);

    appFile.expo.name = AppName+'-dev';
    appFile.expo.scheme = AppName;
    appFile.expo.slug = AppName;
    appFile.expo.version = 0.1;
    appFile.expo.ios.bundleIdentifier = 'com.'+AppName;
    appFile.expo.ios.buildNumber = '0';
    appFile.expo.android.package = 'com.'+AppName;
    appFile.expo.android.versionCode = 0;

    stagingFile.expo.name = AppName+'-staging';
    stagingFile.expo.scheme = AppName;
    stagingFile.expo.slug = AppName;
    stagingFile.expo.version = 0.1;
    stagingFile.expo.ios.bundleIdentifier = 'com.'+AppName;
    stagingFile.expo.ios.buildNumber = '0';
    stagingFile.expo.android.package = 'com.'+AppName;
    stagingFile.expo.android.versionCode = 0;

    prodFile.expo.name = AppName;
    prodFile.expo.scheme = AppName;
    prodFile.expo.slug = AppName;
    prodFile.expo.version = 0.1;
    prodFile.expo.ios.bundleIdentifier = 'com.'+AppName;
    prodFile.expo.ios.buildNumber = '0';
    prodFile.expo.android.package = 'com.'+AppName;
    prodFile.expo.android.versionCode = 0;

    const appPromise = new Promise((resolve, reject) =>{

        fs.writeFile(app, JSON.stringify(appFile, null, 4), function writeJSON(err) {
            if (err) return console.log(err);
            //console.log(JSON.stringify(appFile, null, 4));
            console.log('writing to ' + app);
            resolve(true);
        });
    });
    const stagingPromise = new Promise((resolve, reject) =>{
        fs.writeFile(staging, JSON.stringify(stagingFile, null, 4), function writeJSON(err) {
            if (err) return console.log(err);
            //console.log(JSON.stringify(stagingFile, null, 4));
            console.log('writing to ' + staging);
            resolve(true);
        });
    });
    const productionpPromise = new Promise((resolve, reject) =>{
        fs.writeFile(production, JSON.stringify(prodFile, null, 4), function writeJSON(err) {
            if (err) return console.log(err);
            //console.log(JSON.stringify(prodFile, null, 4));
            console.log('writing to ' + production);
            resolve(true);
        });
    });
    
    const promises = [appPromise,stagingPromise,productionpPromise];
    await Promise.allSettled(promises);
    return;
  
}

async function HandleFirebase(){
    const myPromise = new Promise((resolve, reject) =>{
        console.log('Firebase Login');
        let res2 = execSync("firebase login --interactive", {stdio: 'inherit'});

        console.log('Firebase Initialize');
        // res = execSync("firebase init ", {stdio: 'inherit'});
        const firebaserc = workingDir+'/'+AppName+'/.firebaserc';
        const firebasercFile = {
            "projects": {
            "default": ProjectName
            }
        };
        fs.writeFile(firebaserc, JSON.stringify(firebasercFile, null, 4), function writeJSON(err) {
            if (err) return console.log(err);
            //console.log(JSON.stringify(stagingFile, null, 4));
            console.log('writing to ' + firebaserc);
            try{
                execSync("cd "+AppName+" && firebase use default", {stdio: 'inherit'});
                resolve(true);
            }
            catch(error){
                console.error(error.message);
                resolve(false);
                //reject(error.message);
            }
        });
    });
    await myPromise;
    return;
}

async function InstallNodeModules(){
    const myPromise = new Promise((resolve, reject) => {
        try{
            execSync("cd "+AppName+" && yarn install", {stdio: 'inherit'});
            resolve(true);
        }
        catch(error){
            console.error(error.message);
            resolve(false);
            //reject(error.message);
        }
        finally{
            
        }
    });
    await myPromise;
    return;
}

init();